require('dotenv').config()
const express = require('express');
const app = express();
const server = require('http').Server(app);
const cors = require('cors')

app.use(cors());
app.options('*', cors());
app.use(express.static('public'));


const io = require('socket.io')(server,cors());
io.origins('*:*')

io.on('connection',async (socket) => {
    await city_data_refresh()
    socket.emit('cities', city_data);
});

const port = process.env.PORT
server.listen(port, () => {
    console.log(`running on ${port}`);
});



const {
    process_data
} = require('./bussiness/process')
const {datos} = require('./bussiness/data')

city_data = []
city_data_refresh = async () => {
    city_data = await process_data()
}

setInterval(async () => {
    countUsers = io.engine.clientsCount
    console.log(`Usuarios conectados: ${countUsers}`)
    if (countUsers > 0){
        await city_data_refresh();
        console.log("city_data enviada",city_data)
        io.sockets.emit('messages', city_data);
    }else{
        city_data = [];
    }
}, 10000);

const { setValue_JSON } = require('./data/redis')
setValue_JSON('cities',JSON.stringify(datos))