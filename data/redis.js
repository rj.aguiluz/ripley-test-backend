require('dotenv').config()
const redis = require('redis')

const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_ENDPONT);
client.auth(process.env.REDIS_PW);

client.on('connect', function () {
    console.log('Redis client connected');
});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

setValue_JSON = (key, val) => {
    client.set(key, JSON.stringify(val), redis.print);
}

getValue_JSON = async (key) => {
    value = new Promise((res,rej)=>{
        client.get(key, (error, result) => {
            if (error) {
                console.log(error);
                throw error;
            }

            res(JSON.parse(result));
        });
    })
    return value;
}

saveError = (key,value) => {
    client.hmset(key,Date.now(),value,redis.print)
}

module.exports = {
    setValue_JSON,
    getValue_JSON,
    saveError
}