
const request = require("request");
const { getValue_JSON,saveError } = require('../data/redis')

require('dotenv').config()

call_darksky = async (lat, long) => {
    const endpoint = process.env.FORECAST_IO_ENDPOINT
    const key = process.env.FORECAST_IO_KEY

    const options = {
        method: 'GET',
        url: `${endpoint}/${key}/${lat},${long}`,
        headers: {
            'cache-control': 'no-cache',
            'Content-Type': 'application/json',
        }
    };

    try {
        if (Math.random() < 0.1) throw new Error('How unfortunate!The API Request Failed')
        const response = await new Promise((resolve, reject) => {
            request(options, function (error, response, body) {
                if (error) throw new Error('Error');
                resolve(JSON.parse(body));
            });
        })
        return response
    } catch (e) {
        if (e.message=="How unfortunate!The API Request Failed") {
            saveError("api.errors",e.message)
        }
        return await call_darksky(lat, long)
    }
}


process_data = async () => {
    var aux_data = JSON.parse(await getValue_JSON('cities'))

    for (let i=0;i<aux_data.length;i++){
        d = aux_data[i]
        
        darksky_info = await call_darksky(d.geo.latitude, d.geo.longitude)

        city_data = {
            time: darksky_info.timezone,
            temperatureF: darksky_info.currently.temperature.toFixed(1),
            temperatureC: ((Number.parseFloat(darksky_info.currently.temperature) - 32) * (5 / 9)).toFixed(1)
        }

        d.darksky_data = city_data
    }
    return aux_data
}

module.exports = {
    process_data
}