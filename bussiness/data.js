
module.exports = {
    datos: [
        {
            "city": "Santiago",
            "country": "CL",
            "geo": {
                "latitude": -33.448891,
                "longitude": -70.669266
            }
        },
        {
            "city": "Zurich",
            "country": "CH",
            "geo": {
                "latitude": 47.376888,
                "longitude": 8.541694
            }
        },
        {
            "city": "Auckland",
            "country": "NZ",
            "geo": {
                "latitude": -36.848461,
                "longitude": 174.763336
            }
        },
        {
            "city": "Sydney",
            "country": "AU",
            "geo": {
                "latitude": -33.868820,
                "longitude": 151.209290
            }
        },
        {
            "city": "Londres",
            "country": "UK",
            "geo": {
                "latitude": 51.500149,
                "longitude": -0.126240
            }
        },
        {
            "city": "Georgia",
            "country": "USA",
            "geo": {
                "latitude": 32.165623,
                "longitude": -82.900078
            }
        }
    ]    
}